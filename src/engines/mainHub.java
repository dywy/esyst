package engines;

import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.filefilter.IOFileFilter;



import com.google.common.collect.Multimap;
import com.itextpdf.text.DocumentException;

public class mainHub {
	public static Multimap<String, File> files; //  Google Collections TreeMultimap containing fileKey<string> and Collection<File> 
	public static String inPath;//The path used to look for tiff files
	private static String[][] MObjects = null;
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI.mainGUI frame = new GUI.mainGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
		
	
	/*//============================================================== analyze
	 * This 'analyze' method is meant to be called when the 'analyze tiff files' button is pressed.
	 * Requires String[] inPath and List<String> filterList.
	 * Returns MultMap<String, Collection<File>) files, ready for pdf conversion by the tiff2pdf class
	 */
	public static void analyze(String inPath, List<String> filterList){
		GUI.mainGUI.message("mainHub says: about to create filesFilter.\n");
		
		IOFileFilter filesFilter = engines.FilterBuilder.buildFileFilter(filterList); //Build a filter from filterList. FilterList could be empty!
		GUI.mainGUI.message("mainHub says: here's filesFilter:" + filesFilter.toString()+"\n");
		
		IOFileFilter dirFilter = engines.FilterBuilder.buildDirFilter(filterList); //Build a filter from filterList. FilterList could be empty!
		GUI.mainGUI.message("mainHub says: here's dirFilter:" + dirFilter.toString()+"\n");
		
		GUI.mainGUI.message("mainHub says: I'm gonna get the filenames from DirWalker now..\n");
		GUI.mainGUI.message("...it may not look like it, but I'm working on it...\n");
		DirWalker filesCollector = new engines.DirWalker(dirFilter, filesFilter); //Create fileCollector using the filter
		
		File inDir = new File(inPath); //cast String inPath to the type File, needed by DirectoryWalker.walk method
		List<File> filesCollected= filesCollector.getFiles(inDir);
								
		GUI.mainGUI.message("mainHub says: OK; we gottem. I'm sending the filenames to engines.FileCollection...\n");
		FileCollection filesCollection = new FileCollection(filesCollected);//Send the filenames arrayList to FileCollection class
		
		files = filesCollection.getFiles();//go get a MultiMap of file names and matching pdf file name (fileKey)
		
		
				
	}//end of analyze method
	
	
	//============================================================== getFilesTable
	public static Multimap<String, File> getFilesTable(){
		return files;
	}
	

	/*============================================================== makePDF
	 * This 'makePDF' method is meant to be called when the "make PDF files" button is pressed.
	 * Requires String outPath.
	 * Uses ArrayList<ArrayList<String>> files. When method analyze hasn't run, contents of  files is null.
	 * In that case, through a error.
	 */
	public static void makePDF(String outPath, Multimap<String, File> files){
		File outDir= new File(outPath);
		
		//tiff2pdf pdfs = new engines.tiff2pdf(); 
		//Now generate the actual pdf files
		try {
			engines.tiff2pdf.makePDFs(outDir, files);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//end of makePDF method
	
	
	/*======= The below methods are related to dealing with M-Files ====================
	 * ============================================================================
	 
	 * ============================================================= parseMFile 
	 *This method will be run when the Parse MFile button is pressed.
	 */
	public static void parseMFile(String MFile){
		GUI.mainGUI.message("Receive folowing as an M-File :" + GUI.MFileParserPanel.MFilePath.getText()+"\n"); //User Info
		processMFile parsedMFile = new processMFile(); //Make new processMFile instance
		MObjects= parsedMFile.parseMFile(GUI.MFileParserPanel.MFilePath.getText()); //Call parseMFile whilst sending MFilePath String
		//Send MObjects to JTable MFileParsedTable for display
		//GUI.mainGUI.updateMFileParsedTable(MObjects); //TODO; how to do this?
		//GUI.mainGUI.MFileParsedTable=MObjects;
		GUI.MFileParserPanel.MFileParsedTable.repaint();
		GUI.mainGUI.message("Called GUI.mainGUI.MFileParsedTable.repaint()\n");
		GUI.mainGUI.message("MObjects looks like this: " + MObjects.length + " by "+ MObjects[0].length + "\n");
	} 
	/*//============================================================== xlsMFile
	 * 
	 *This method will be run when the SaveMFileXLS is pressed.
	 */
	public static void xlsMFile() {
		processMFile parsedMFile = new processMFile(); //Make new processMFile instance
		parsedMFile.xlsMfile(MObjects, "defaultMFileName.xls"); //TODO: add 'save/as' or so.
	}//end of xlsMFile method
}
