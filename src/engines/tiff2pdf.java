package engines;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
//import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.pdf.codec.TiffImage;

import com.google.common.collect.Multimap;

//To write the final PDF file which has our image converted to PDF
//Document object to add logical image files to PDF
//The image class to extract separate images from Tiff image
//PdfWriter object to write the PDF document
//This object will hold our Tiff File
//Read Tiff File, Get number of Pages

public class tiff2pdf extends ArrayList<Object> {
		
	private static final long serialVersionUID = 5090766234944188609L;
	static Multimap<String, File> fileTable;
	private static File outPath;

	//=================================================== makePDFs
	public static void makePDFs(File theoutPath, Multimap<String,File> thefileTable) throws DocumentException,
		IOException {
		outPath=theoutPath;//where to put the pdf files
		fileTable = thefileTable;//an ArrayList of Arraylists holding the file names. 
		Document pdfDoc = null;//the pdf doc object as used by for/if loops below.
		File pdfFile = null;//the pdf file object as used by for/if loops below.
		
		for(String key : fileTable.keySet()) {//This loop will run through the fileTable, key by key
				
								
			ArrayList<File> tiffs=new ArrayList<File>(fileTable.get(key)); //The files associated with the key are placed in a ArrayList<File> 'tiffs'
			System.out.println("tff2pdf says: I put the tiff files for " + key + " in a ArrayList: "+ tiffs);
			
			//for(Iterator<File> tiff = tiffs.listIterator(tiffs.size()); tiff.hasNext();){ //This loop will run along the files one-by-one
			
			for(int j=0; j<tiffs.size();j++){
							
				//File tifFile = tiff.next(); //The tiff file at-hand is assigned to 'file'
				
				File tifFile = tiffs.get(j);
				
				System.out.println("tff2pdf says: looking at" + tifFile);
			
				RandomAccessFileOrArray myTiffFile = new RandomAccessFileOrArray(tifFile.getAbsolutePath()); //The tiff file, 'file', is read (as a String).
							
				for (int i = 1; i <=TiffImage.getNumberOfPages(myTiffFile); i++) {// Loop through the individual tiff IMAGES in case of a multi-paged tiff file. 
					Image image = TiffImage.getTiffImage(myTiffFile, i);//get image
					tiffInfo pageInfo = new tiffInfo();//define a pageInfo of type tiffInfo
					Rectangle pageSize = new Rectangle(pageInfo.getPaperGeom(image));//Create pageSize

					
					//If this is the very first image for this pdf (j==1), start the document:
					if (j==0&i==1){
						pdfDoc = new Document(pageSize);
						pdfFile = new File(outPath, key + ".pdf"); //Define a new pdfFile using the outPath, the key and concatenate .pdf
						FileOutputStream pdfOutStream = new FileOutputStream(pdfFile); //Create outputstream using the pdf File
						PdfWriter writer = PdfWriter.getInstance(pdfDoc, pdfOutStream); //Create a PDFWriter instance
						writer.setStrictImageSequence(true);
						pdfDoc.open();
						System.out.println("tff2pdf says:  Opened a pdf doc called " + pdfFile + ", " + pdfDoc.getPageSize().getWidth() + " x " + pdfDoc.getPageSize().getHeight());
						
						prepImage(image, pdfDoc);//This method preps the image

						pdfDoc.add(image);
						System.out.println("tff2pdf says: Added image "+ tifFile.getName() +"("+ image.getWidth()+" x "+image.getHeight() + ") to " + pdfFile.getName());
																
					}//End of if-statement
						
					//Else, the document is already open, add a page, and then Scale, position and add the image.
					else {
						pdfDoc.newPage();				
						prepImage(image, pdfDoc);//This method preps the image
						
						pdfDoc.add(image);
						System.out.println("tff2pdf says: Added a page with image "+ tifFile.getName()
								+ "("+ image.getWidth()
								+" x "
								+image.getHeight() +
								") to " + pdfFile.getName());
						}//End of else-statement
								
				}//end of the i-loop, which runs through the individual tiff images embedded in a single file.
			
			}//end of for(ListIterator<File> tiff... which runs through every tiff file for a certain key
	
			pdfDoc.close();//close the doc
			
		}// end of for(String key : fileTable.keySet()... which runs along every key

	} //end of IOException
	
	public static Image prepImage(Image image, Document document){
				
		//image.setAlignment(Image.MIDDLE);
		//image.scaleAbsolute(document.getPageSize().getWidth(), document.getPageSize().getHeight());
		image.scaleToFit(document.getPageSize().getWidth(), document.getPageSize().getHeight());
		//image.rotate();
		//image.setRotationDegrees(0);
		image.setAbsolutePosition(0, 0);
		image.setCompressionLevel(0);
		System.out.println("tff2pdf says: Prepared image for PageSize "+ document.getPageSize().getWidth() + " x "+ document.getPageSize().getHeight());
		return image;
	}//end of prepImage method
	
}//end of tiff2PDF class
