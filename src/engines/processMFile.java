package engines;

import java.io.*;
import java.util.*;
import org.apache.commons.io.FileUtils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;


public class processMFile {

	List<String> lines = new ArrayList<String>();//An ArrayList of Strings to hold the file's individual lines.
	
	
	public String[][] parseMFile(String MFile){
		try{
			lines = FileUtils.readLines(new File(MFile)); //read lines using apache commons utils.
		}
		catch(Exception e){
		 System.err.println("Error: " + e.getMessage());
		}
		
		String[][] MObjects= new String[lines.size()][48];
		GUI.mainGUI.message("Parsing "+ MFile +" to an array of "+ MObjects.length + " by "+ MObjects[0].length);
		
		for(int i=0; i<MObjects.length-1; i++){
			
			String str0In = lines.get(i);
			if(str0In.substring(13,14).toUpperCase().matches(".*(P|G)")){
				MObjects[i][0]=str0In.substring(13,13+5)+"-"+str0In.substring(13+5,13+5+3) +"-" +str0In.substring(13+5+3,13+15).trim();
			}
			else{
				MObjects[i][0]=str0In.substring(13,13+5)+"-"+str0In.substring(13+5,13+5+10).trim();//ITEM-NO, start@13, length=15, with "-" inserted.
			}
			
			MObjects[i][1]=lines.get(i).substring(28,28+30).trim();//ITEM-DESC, start@28, length=30
			MObjects[i][2]=lines.get(i).substring(58,58+3).trim();//REVISION-NO, start@58, length=3
			MObjects[i][3]=lines.get(i).substring(61,61+3).trim();//UOM-DIM, start@61, length=3
			
			MObjects[i][29]=lines.get(i).substring(241,241+5)+"-"+ lines.get(i).substring(241+5,241+5+5).trim();//DRAW-NO, start@241, length=10, with "-" inserted.
			
			}
		return MObjects;
	}
	
	public void printMFile(String[][] MObjects){
		for (int i=0 ; i<MObjects.length;i++){
				//System.out.println("Line "+ i +" from file: " + lines.get(i));
				System.out.println(" ->Parsed values for line " + i +
						" : item " + MObjects[i][0]+ ", " + MObjects[i][1] +
						" , Desribed by: " + MObjects[i][29] + " R" + MObjects[i][2]);
		}
	}
	
	public void xlsMfile(String[][] MObjects, String fileName){
		try {
			FileOutputStream fileOut = new FileOutputStream(fileName);
			HSSFWorkbook workbook = new HSSFWorkbook();
			Sheet worksheet = workbook.createSheet("Parsed file ");

			for (int i=0;i<MObjects.length;i++){
			Row rowi = worksheet.createRow((short) i);

				for (int j=0; j<MObjects[0].length;j++){
				Cell celli = rowi.createCell((short) j);
				celli.setCellValue(MObjects[i][j]);
				}
			}
			workbook.write(fileOut);
			System.out.print("Writing file "+ fileName + " ...");
			fileOut.flush();
			fileOut.close();
			System.out.println("...closed file");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
