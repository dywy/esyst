package engines;


import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;


//===================================================tiffInfo constructor
public class tiffInfo {
	
	public Rectangle getPaperGeom(Image image){
			    
		double paperWidth = Math.round(25.4 * image.getWidth()/image.getDpiX());//width in mm
		double paperHeight = Math.round(25.4 * image.getHeight()/image.getDpiY());//height in mm
		double sizeTol=10; //this is a value for the tolerance in finding page sizes
		Rectangle paperGeom;
		
		
		if (Math.abs(paperWidth - 1189) <sizeTol && Math.abs(paperHeight - 841) <sizeTol){//A0L
		paperGeom= PageSize.A0.rotate();
		}
		else if (Math.abs(paperWidth - 841) <sizeTol && Math.abs(paperHeight - 1189) <sizeTol){//A0P
			paperGeom=PageSize.A0;
		}
		else if (Math.abs(paperWidth - 841) <sizeTol && Math.abs(paperHeight - 594) <sizeTol){//A1L
			paperGeom=PageSize.A1.rotate();
		}
		else if (Math.abs(paperWidth - 594) <sizeTol && Math.abs(paperHeight - 841) <sizeTol){//A1P
			paperGeom=PageSize.A1;
		}
		else if (Math.abs(paperWidth - 594) <sizeTol && Math.abs(paperHeight - 420) <sizeTol){//A2L
			paperGeom=PageSize.A2.rotate();
		}
		else if (Math.abs(paperWidth - 420) <sizeTol && Math.abs(paperHeight - 594) <sizeTol){//A2P
			paperGeom=PageSize.A2;
		}
		else if (Math.abs(paperWidth - 420) <sizeTol && Math.abs(paperHeight - 297) <sizeTol){//A3L
			paperGeom=PageSize.A3.rotate();
		}
		else if (Math.abs(paperWidth - 297) <sizeTol && Math.abs(paperHeight - 420) <sizeTol){//A3P
			paperGeom=PageSize.A3;
		}
		else if (Math.abs(paperWidth - 297) <sizeTol && Math.abs(paperHeight - 210) <sizeTol){//A4L
			paperGeom=PageSize.A4.rotate();
		}		
		else if (Math.abs(paperWidth - 210) <sizeTol && Math.abs(paperHeight - 297) <sizeTol){//A4P
			paperGeom=PageSize.A4;
		}
		else if (Math.abs(paperWidth - 210) <sizeTol && Math.abs(paperHeight - 148) <sizeTol){//A5L
			paperGeom=PageSize.A5.rotate();
		}
		else if (Math.abs(paperWidth - 148) <sizeTol && Math.abs(paperHeight - 210) <sizeTol){//A5P
			paperGeom=PageSize.A5;
		}		

				
		else { //If no fit is found with in the define margin, revert to using the calculated measurements
			 paperGeom= new Rectangle((float)paperWidth, (float) paperHeight);
		}
		
		return paperGeom;
	
	}

	//=================================================== printTiffInfo
	public void printTiffInfo(Image image){
		
		System.out.println("Width: " + image.getWidth() + " Height: " + image.getHeight());
		System.out.println("DPI X: " + image.getDpiX() + "; DPI Y: " + image.getDpiY());
		System.out.println("Dimension: " + image.getWidth()/image.getDpiX() +"\" by " + image.getHeight()/image.getDpiY() + "\"");
		System.out.println("Dimension: " + 25.4 * image.getWidth()/image.getDpiX() +"\" by " + 25.4 * image.getHeight()/image.getDpiY() + "\"");
		System.out.println("Probable A-size, if any:" + getPaperGeom(image));
	}
		
}
