 package engines;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;


public class FileCollection extends ArrayList<Object> {
	
	private static final long serialVersionUID = -6691102630339641916L;
	
	Multimap<String, File> fileTable = TreeMultimap.create(Ordering.natural(), Ordering.natural());
		
	//============================================================== Constructor
	public FileCollection(List<File> filesList) {
		
		System.out.println("FileCollection constructor says: Yes, master?");
		System.out.println("FileCollection constructor says: I received fileList with this in it:");
		for(int i=0; i<filesList.size();i++ ){
			System.out.println(filesList.get(i));
		}
		
		//Going to go _UP_ the filesList using a while(hasPrevious) ListIterator, because that will run faster when removing objects
		for(ListIterator<File> iter = filesList.listIterator(filesList.size()); iter.hasPrevious();) {
				
				File fileName = iter.previous(); //assign the previous file name in the List to a temp name			
				String fileKey= FileCollection.getFileKey(fileName); //Send the filename over to the getFileSetKey method to get the fileSetKey
				
				System.out.println("FileCollection constructor says: I sent " + fileName.getName() +" for analysis and got: "+ fileKey);
								
				fileTable.put(fileKey, fileName);
								
			}//done running _up_ the 'files' ArrayList

	}//FileCollection constructor method ends here
	
	
	//============================================================== getFileKey
	public static String getFileKey(File file){
		
		/* Of the Array, column[0] should will show the pdf filename.
		file naming conventions:
		1. MHI drawings:
		1a. <10characters>-<page>.tif for the first rev (0)
		1b. <10characters>-<page>-<rev>.tif for rev 1 and up
		1c. <10characters>.tif for the first rev (0); this from may be illegal, but is found sometimes
		2. MHI MSJ spec: MSJ<1,2,3,><4digit><E, more?>
		*/

		String fileSetKey = new String(); //A String to be built from the filename. Several files may share a single fileSet.
		
		//***** These statements will to check for files of forms 1a, 1b and 1c, and act accordingly*******
		// Check if the file name constitutes a drawing (a "-" (form 1a or 1b), or a "." (form 1c) on pos 11 (10 in the array): 
		if (file.getName().indexOf("-") == 10 | file.getName().indexOf(".")==10 ) {
			
			//**** Check for form 1a or 1b:
			// if the 'dot' is on pos 15 (14 in the array) or pos 11 (10 in the array) , it's of form 1a or 1c resp. Both 1a and 1c are named equally.
			if (file.getName().indexOf(".")==14 | file.getName().indexOf(".")==10 ){
				
			//Build a fileSet ID by concatenating the first 5 characters of filename, "-" and from 6th character until first ".".
				
				fileSetKey = file.getName().substring(0, 5) //first 5 characters (0~4)
							+ "-"+ //the '-' character in the partnumber
							file.getName().substring(5,10);  //Characters 6 to 10 (5~9)
				System.out.println("FileCollection.getFileKey says: I found a filename that looks like a drawing with no revision."+ fileSetKey);
			}// end of Check for form 1a.
				
			// There's no "." on pos 14 or pos 10, so this must be a drawing of form 1b.
			else{ 
			//Build a fileSet ID by concatenating the first 5 characters of filename, "-" and from 6th character until first "-".
				fileSetKey = (
						file.getName().substring(0, 5) //first 5 characters (0~4)
						+ "-" //the '-' character in the partnumber
						+ file.getName().substring(5,10)  //Characters 6 to 10 (5~9)
						+ "-" //the "-" to seperate the partnumber from rev 
						+ file.getName().substring( (file.getName().lastIndexOf("-")+1), (file.getName().indexOf(".") ) )
					);
				System.out.println("FileCollection.getFileKey says: I found a filename that looks like a drawing with a revision."+ fileSetKey);
			}// end of Check for form 1b
		
		} //end of if statement for drawing filename checking
		
		
		//This else-if-statement will check for the string 'MS' at the start of the file name, constituting a M*-spec
		else if (file.getName().toUpperCase().indexOf("M") == 0){
			
			//*Build a fileSet ID by concatenating the first 4 characters of filename, "-" and the remaining characters.
			fileSetKey = (
					file.getName().substring(0, 4) //first 4 characters (0~3)
					+ "-"+ //the '-' character in the number
					file.getName().substring(4, 7)  //Characters 5 to 8 (4~7)				
				);
			System.out.println("FileCollection.getFileKey says: I found a filename that looks like an M*-spec"+ fileSetKey);
		}
		
		else {
			//If the filename is not within the above possibilities, we don't know what to do with it,
			// return the original filename and append the system time as a fall-back (note this is +-1ms, so maybe not 100% unique)
			fileSetKey =  file.getName() + "-" + System.currentTimeMillis();
		
			}
		
		return fileSetKey;
		
	}//getFileSetKey method ends here.
		
	//============================================================== getFiles
	public Multimap<String, File> getFiles(){
		 System.out.println("FileCollection.getfiles says: I am returning TreeMultimap<String,File> which has "+ fileTable.size() +" files:");
		 for(String key : fileTable.keySet()) {
		 System.out.println(key + ": " + fileTable.get(key));
		 }
		return fileTable;
	}//getFiles method ends here
	
}//FileCollection class ends here	.
	
