package engines;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.DirectoryWalker;
import org.apache.commons.io.filefilter.IOFileFilter;


public class DirWalker extends DirectoryWalker<File>{
	
    public DirWalker(IOFileFilter dirFilter , IOFileFilter fileFilter) {
        super(dirFilter , fileFilter , -1);
      System.out.println("DirWalker.getFiles says: Thy command?");
      System.out.println("DirWalker.getFiles says: I'll be using a filter: " + fileFilter);
   	}
    
    public List<File> getFiles(File inDir){
    	List<File> results = new ArrayList<File>();
    	
    	try {
    		System.out.println("DirWalker.getFiles says: I shall walk through " + inDir);
			walk(inDir, results);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
     	System.out.println("DirWalker.getFiles says: I am returning List<File> results:");
     	for(int i=0;i<results.size();i++){
     		System.out.println(results.get(i));
     	}
    	return results;
    }

    //This method must be here to override the one inside DirectoryWalker
    protected void handleFile(File file, int depth, Collection<File> results) throws IOException {
    	//System.out.println("DirWalker.getFiles says: I Found a file: " + file.getAbsolutePath());
    	results.add(file);
    	
    }
    
    //This method must be here to override the one inside DirectoryWalker
    protected boolean handleDirectory(File dir, int depth, Collection<File> results) throws IOException {
    	//System.out.println("DirWalker.getFiles says: I Found a dir: " + dir.getAbsolutePath());
    	return true;
    }
    
    
}

	


