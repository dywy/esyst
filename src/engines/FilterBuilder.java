package engines;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;


public class FilterBuilder {
	
List<String> filterList = new ArrayList<String>(); //= Arrays.asList("*");
	
	/*==============================================================parseFilterFile
	* This method will read the lines of a txt file into an List<String>
	 */
	public List<String> parseFilterFile(String filterFile) {
		System.out.println("FilterBuilder.parseFilterFile says: I was called");
		try{
			filterList = FileUtils.readLines(new File(filterFile)); //read lines using apache commons utils.
		}
		catch(Exception e){
		 System.err.println("Error: " + e.getMessage());
		}
		return filterList; //TODO: maybe type List<String> is NG, then add toArray or so.
	}
	
	/*==============================================================buildFileFilter
	/*This method will return a filter depending on the value of boolean useFilter.
	 * If filterList is empty, it will create just a SuffixFilter filter  for .tiff or .tif 
	 * If filterList is not empty, it will combine the SuffixFilter with a WildCardFilter, which takes List<String> filterList
	 * The method parseFilter, included in this class, may be used to create contents for filterList
	 */
	public static IOFileFilter buildFileFilter(List<String> filterList){
	
		SuffixFileFilter suffixFilter = new SuffixFileFilter(new String[] {"tiff","tif"},IOCase.INSENSITIVE);//Create a suffix filter
		IOFileFilter filter;
				
		if(filterList.isEmpty()){		
			System.out.println("FilterBuilder.buildFilter says: filterList is empty, so just make SuffixFilter");
			filter = suffixFilter;
		}
		
		else{
			System.out.println("FilterBuilder.buildFilter says: filterList is not empty, so use it.");
			System.out.println("FilterBuilder.buildFilter says: About filterList: "+filterList.size());
			WildcardFileFilter namesFilter = new WildcardFileFilter(filterList);//Create a filter from the filterList
			filter= FileFilterUtils.and(namesFilter, suffixFilter); //Combine the two filters
		}
		
		System.out.println("FilterBuilder.buildFilter says: returning filter");	
		return filter;
	}
	
	
	public static IOFileFilter buildDirFilter(List<String> filterList){
		IOFileFilter dirFilter= new WildcardFileFilter("*");
		return dirFilter;
	}
	

	
}
