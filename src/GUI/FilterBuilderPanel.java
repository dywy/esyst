package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class FilterBuilderPanel extends JPanel{

	private static final long serialVersionUID = -8131464888451375219L;
	
	private JTextField filterFilePath;
	static JTextArea manualFilterPane = new JTextArea();
	
	public FilterBuilderPanel(){
		
		setLayout(null);
		JButton btnSelectFilterFile = new JButton("Use a File:");
		btnSelectFilterFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mainGUI.message("Opening FileChooser... \n");
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Select Filter File");
				//chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);
				
					if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
						filterFilePath.setText(chooser.getSelectedFile().getPath());
						mainGUI.message("Selected Filter File: " +  filterFilePath.getText() + "\n");
						}
					else {
						mainGUI.message("... canceled \n");
						}				
				
			}
		});
		btnSelectFilterFile.setToolTipText("Click to select");
		btnSelectFilterFile.setBounds(12, 12, 117, 25);
		this.add(btnSelectFilterFile);
		
		filterFilePath = new JTextField();
		filterFilePath.setText("fileFilterFile.txt");
		filterFilePath.setColumns(10);
		filterFilePath.setBounds(141, 15, 239, 19);
		this.add(filterFilePath);
		
		JButton btnParseFile = new JButton("Parse File");
		btnParseFile.setBounds(392, 12, 117, 25);
		this.add(btnParseFile);
		
		JButton btnSaveFile = new JButton("Save as txt");
		btnSaveFile.setBounds(521, 12, 117, 25);
		this.add(btnSaveFile);
		
		JButton btnSaveAsXls = new JButton("Save as xls");
		btnSaveAsXls.setBounds(652, 12, 117, 25);
		this.add(btnSaveAsXls);
		
		JLabel lbl_2a = new JLabel("This text may be used as a filter for tiff2pdf. Use EOL('enters'), not commas, tabs or spaces.");
		lbl_2a.setBounds(12, 49, 757, 19);
		this.add(lbl_2a);
		
		JLabel lbl_2b = new JLabel("Allowed is plain text, * and ? Just * will effectivly disable filtering.");
		lbl_2b.setBounds(12, 69, 757, 15);
		this.add(lbl_2b);
		
		JTextArea filterEditorPane = new JTextArea();
		filterEditorPane.setEditable(false);
		filterEditorPane.setText("*NOT IN USE YET*");
		filterEditorPane.setToolTipText("Filter used by tff2pdf will show here");
		filterEditorPane.setBounds(531, 96, 201, 164);
		this.add(filterEditorPane);
		
		JLabel lblParsedFile = new JLabel("Parsed file:");
		lblParsedFile.setBounds(521, 64, 88, 25);
		this.add(lblParsedFile);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(157, 96, 204, 164);
		this.add(scrollPane);
		scrollPane.setViewportView(manualFilterPane);
		manualFilterPane.setToolTipText("Filter used by tff2pdf will show here");
		manualFilterPane.setText("*");
		
		final JCheckBox chckbxAddAsterisks = new JCheckBox("Add Asterisks");
		chckbxAddAsterisks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				List<String> filterList = Arrays.asList(manualFilterPane.getText().split("[\\r\\n]+"));;//get the filterList from the JTextArea manualFilterPane
				if (chckbxAddAsterisks.isSelected()==true){
					manualFilterPane.setText("");
					for(int i=0;i<filterList.size();i++){
						manualFilterPane.append("*" + filterList.get(i) + "*\r\n");
					}
				}
				else{
					manualFilterPane.setText("");
					for(int i=0;i<filterList.size();i++){
						manualFilterPane.append(filterList.get(i).replace("*", "") + "\r\n");
					}	
				}
			}
		});
		chckbxAddAsterisks.setBounds(12, 97, 129, 23);
		this.add(chckbxAddAsterisks);		
		
	}
	
}
