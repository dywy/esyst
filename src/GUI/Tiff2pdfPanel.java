package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import engines.mainHub;

public class Tiff2pdfPanel extends JPanel{
	
	private static final long serialVersionUID = 5702111871752524219L;
	
	private JTextField inPathField;
	private JTextField outPathField;

	public Tiff2pdfPanel()  {
		
		setLayout(null);
		
		inPathField = new JTextField();
		inPathField.setText("");
		inPathField.setBounds(140, 10, 377, 19);
		this.add(inPathField);
		inPathField.setColumns(10);
		
		outPathField = new JTextField();
		outPathField.setText("");
		outPathField.setColumns(10);
		outPathField.setBounds(141, 62, 376, 19);
		this.add(outPathField);
		
		//Select input dir button
		JButton btnSelectInputDir = new JButton("Input Dir:");
		btnSelectInputDir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mainGUI.message("Opening FileChooser... \n");
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Select Input Dir");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);
				
				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
					inPathField.setText(chooser.getSelectedFile().getPath());
					mainGUI.message("Inpath field set to: " +  outPathField.getText() + "\n");
				}
				else {
					mainGUI.message("... canceled \n");
				}
			}
		});
		
		btnSelectInputDir.setToolTipText("Not working yet. Please type in path manually");
		btnSelectInputDir.setBounds(12, 7, 117, 25);
		this.add(btnSelectInputDir);
		
		//Select output dir button
		JButton btnSelectOutputDir = new JButton("Output Dir:");
		btnSelectOutputDir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mainGUI.message("Opening FileChooser... \n");
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Select Output Dir");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);
				
					if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
						outPathField.setText(chooser.getSelectedFile().getPath());
						mainGUI.message("Outpath field set to: " +  outPathField.getText() + "\n");
						}
					else {
						mainGUI.message("... canceled \n");
						}
			}
		});
		
		btnSelectOutputDir.setToolTipText("Select path to place files");
		btnSelectOutputDir.setBounds(12, 59, 117, 25);
		this.add(btnSelectOutputDir);
		
		//==Use filter check box
		final JCheckBox chckbxUseFilter = new JCheckBox("Use filter (use the Filter Builder Tab)");
		chckbxUseFilter.setBounds(140, 35, 283, 21);
		this.add(chckbxUseFilter);

		
		//==files table JTable in JScrollPane
		//Useful: http://www.chka.de/swing/
		JScrollPane tableScrollPane = new JScrollPane();
		tableScrollPane.setBounds(12, 96, 757, 164);
		this.add(tableScrollPane);
		
	
		final DefaultTableModel tableModel = new DefaultTableModel();
		tableModel.addColumn("Key");
		tableModel.addColumn("tiff Files");
		
		
		final JTable tiff2pdfTable = new JTable(tableModel);
		tiff2pdfTable.setToolTipText("Results will be shown here");
		tiff2pdfTable.setColumnSelectionAllowed(true);
		tiff2pdfTable.setCellSelectionEnabled(true);
		tiff2pdfTable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);//else, there won't be a horizontal scroll bar
		//tiff2pdfTable.doLayout();
		
		
		tableScrollPane.setViewportView(tiff2pdfTable);
		
				
		//==Analyze button
		JButton btnAnalyzeFiles = new JButton("Analyze");
		btnAnalyzeFiles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainGUI.message("Alright; the analyze button was pushed.\n");
				mainGUI.message("I have this for inPath:\n");
				mainGUI.message(inPathField.getText()+ "\n");
				
				List<String> filterList = new ArrayList<String>();
				if(chckbxUseFilter.isSelected() !=false){//If  useFilter = true  (chckbxUseFilter.isSelected() is true),
													
					mainGUI.message("useFilter is true, so I will collect and send filter values\n");
					filterList = Arrays.asList(FilterBuilderPanel.manualFilterPane.getText().split("[\\r\\n]+"));
					mainGUI.message("I got this for filterList:\n");
					for(int i=0;i<filterList.size();i++){
						mainGUI.message(filterList.get(i)+"\n");
					}
				}
				else{
					//System.out.println("mainGUI btnAnalyzeFiles.addActionListener says: useFilter is false, so leaving filterList as it is. ");
					mainGUI.message("useFilter is false, so not using it.\n");
				}
				
				
				mainGUI.message("I'm going to call engines.mainHub.analyze, using that path and filter options. \n");
				
				engines.mainHub.analyze(inPathField.getText(), filterList);//Call the engines.mainHub.analyze method using the stuff found above
						
				
				//==Display the results in the table 
				mainGUI.fileTable=mainHub.getFilesTable(); //get the results to be displayed
				mainGUI.message("OK; I got the results back and will display in the table above. \n");
				mainGUI.message("If it looks alright and the outpath is set, you could consider making pdfs now.\n");
				
				tableModel.setRowCount(0); //Clear the current contents in the table
				
				int i = 0;
				tiff2pdfTable.setEnabled(false);//enable editing of the table (may have been disabled earlier)
				for(String key : mainGUI.fileTable.keySet()){
						StringBuffer tiffsString = new StringBuffer();
						ArrayList<File> tiffs=new ArrayList<File>(mainGUI.fileTable.get(key)); //The files associated with the key are placed in a ArrayList<File> 'tiffs'
						for(int j=0;j<tiffs.size();j++){//Run along every object associated with this key (i.e., the tiff files)
							if(j>0){
								tiffsString.append(", ");
								tiffsString.append(tiffs.get(j).getName().toString());
							}//end of if j>0 -loop (to add a "," _between_ file names.
							else{
							tiffsString.append(tiffs.get(j).getName().toString());
							}
						}//end of j-loop
						tableModel.addRow(new Object[] {key, tiffsString.toString()});
						i=i+1;
				}// end of fileTable loop
				tiff2pdfTable.doLayout();//column resizing et al.
				tiff2pdfTable.setEnabled(false);//disable editing of the table
			}//end of  actionPerformed
		});
		
		btnAnalyzeFiles.setToolTipText("");
		btnAnalyzeFiles.setBounds(529, 7, 110, 25);
		this.add(btnAnalyzeFiles);
		
		//==MakePDF Button
		JButton btnMakePdf = new JButton("Make PDF");
		btnMakePdf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//When pressed, call the GUI.makePDF method.
				engines.mainHub.makePDF(outPathField.getText(), mainHub.files);
			}
		});
		btnMakePdf.setToolTipText("");
		btnMakePdf.setBounds(529, 59, 110, 25);
		this.add(btnMakePdf);
		
				
		//==Copy as-is button	
		JButton btnCopyAsis = new JButton("Copy as-is");
		btnCopyAsis.setToolTipText("Not implemented");
		btnCopyAsis.setBounds(651, 59, 110, 25);
		this.add(btnCopyAsis);
		
		//==Abort analysis button
		JButton btnAbortAnalyze = new JButton("Abort");
		btnAbortAnalyze.setToolTipText("not working yet. You'll have to wait or kill the process...");
		btnAbortAnalyze.setBounds(651, 7, 110, 25);
		this.add(btnAbortAnalyze);
			
		
		
	}

}
