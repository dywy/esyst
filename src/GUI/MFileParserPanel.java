package GUI;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.eclipse.wb.swing.FocusTraversalOnArray;

public class MFileParserPanel extends JPanel{

	private static final long serialVersionUID = -6552082693361896412L;

	public static JTable MFileParsedTable;
	public static JTextField MFilePath;
	
	public MFileParserPanel(){
		setLayout(null);
		
		//btnSelectMfile===================== MFileParserTab
		JButton btnSelectMfile = new JButton(" M-File:");
		btnSelectMfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainGUI.message("Opening FileChooser... \n");
				JFileChooser chooser = new JFileChooser();
			    FileNameExtensionFilter filter = new FileNameExtensionFilter("text Files", "txt");
			    chooser.setFileFilter(filter);
			    int returnVal = chooser.showOpenDialog(null);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			    	MFilePath.setText(chooser.getSelectedFile().getPath());
			      	mainGUI.message("File selected: " +  MFilePath.getText() + "\n");
						}
					else {
						mainGUI.message("... canceled \n");
						}
							
			}
		});
		btnSelectMfile.setToolTipText("Click to select");
		btnSelectMfile.setBounds(12, 12, 117, 25);
		this.add(btnSelectMfile);
		
		
		
		//MFilePath===================== MFileParserTab
		MFilePath = new JTextField();
		MFilePath.setColumns(10);
		MFilePath.setBounds(141, 15, 374, 19);
		this.add(MFilePath);
		
		JButton ParseMFile = new JButton("Parse File");
		ParseMFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				engines.mainHub.parseMFile(MFilePath.getText());
			}
		});
		ParseMFile.setBounds(527, 12, 117, 25);
		this.add(ParseMFile);
		
		JButton SaveMFileXLS = new JButton("Save as xls");
		SaveMFileXLS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				engines.mainHub.xlsMFile();
			}
		});
		SaveMFileXLS.setBounds(656, 12, 117, 25);
		this.add(SaveMFileXLS);
		
		
		MFileParsedTable = new JTable();
		MFileParsedTable.setToolTipText("Results will be shown here");
		MFileParsedTable.setBounds(12, 46, 757, 225);
		this.add(MFileParsedTable);
		this.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{MFileParsedTable, btnSelectMfile, MFilePath, ParseMFile, SaveMFileXLS}));
				
		
	}
	
	
}
