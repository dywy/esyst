package GUI;

import javax.swing.JPanel;
import javax.swing.JTextPane;

public class WindchillPanel extends JPanel{
	
	private static final long serialVersionUID = 6230864086495920396L;

	public WindchillPanel(){
		
		setLayout(null);
		
		JTextPane txtpnTbd = new JTextPane();
		txtpnTbd.setText("TBD. One idea is to create im/export tools. e.g., create an importable xls from M-file, or to read GCP xls files and colect tiffs, create windchill import sheets, etc.  There is some code/methods, but not yet available.");
		txtpnTbd.setEnabled(false);
		txtpnTbd.setEditable(false);
		txtpnTbd.setBounds(183, 65, 309, 97);
		this.add(txtpnTbd);
	}

}
