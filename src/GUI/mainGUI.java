package GUI;

import java.awt.Component;
import java.io.File;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import org.eclipse.wb.swing.*;

import com.google.common.collect.Multimap;


public class mainGUI extends JFrame {

	private static final long serialVersionUID = 1754935495735572102L;
	
	private JPanel contentPane;

	
	static List<String> manualFilter;
	static Multimap<String, File> fileTable;
	static JTextArea messageTextArea = new JTextArea();
		
	public mainGUI() {
		setTitle("ESYST - the MCFE Engineering Systems Tool");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 458);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//====================================TabbedPane
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(5, 5, 786, 299);
		contentPane.add(tabbedPane);
				
		//==================================== MessageArea
		JScrollPane messageAreaScrollPane = new JScrollPane();
		messageAreaScrollPane.setBounds(5, 312, 786, 111);
		contentPane.add(messageAreaScrollPane);
				
		messageAreaScrollPane.setViewportView(messageTextArea);
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{contentPane, tabbedPane}));
		messageTextArea.setEditable(false);
	
		//====================================tiff2pdfTab
		JPanel tiff2pdfTab = new Tiff2pdfPanel();
		tabbedPane.addTab("tiff2pdf", null, tiff2pdfTab, null);

		
		//==================================== filterBuilderTab
		JPanel filterBuilderTab = new FilterBuilderPanel();
		tabbedPane.addTab("Filter Builder", null, filterBuilderTab, null);
		
		
		//==================================== MFileParserTab
		JPanel MFileParserTab = new MFileParserPanel();
		tabbedPane.addTab("M-File parser", null, MFileParserTab, null);
		
		
		//==================================== WindchillTab
		JPanel WindchillTab = new WindchillPanel();
		tabbedPane.addTab("Windchill Tools", null, WindchillTab, null);

				
		//==================================== ftpTab
		JPanel ftpTab = new FtpPanel();
		tabbedPane.addTab("ftp", null, ftpTab, null);
		
		
		//==================================== dbTab		
		JPanel dbTab = new DbTab();
		//JPanel dbTab = new JPanel();
		tabbedPane.addTab("DB", null, dbTab, null);

		
	}
	
	//TODO: the line limitation mechanism isn't working
	public static void message(String text) {
		if(messageTextArea.getLineCount()<2000){
			messageTextArea.append(text);
			}
		else{
			System.out.println("message text area max lines exceeded");
			try {
				messageTextArea.replaceRange("",messageTextArea.getLineStartOffset(0),messageTextArea.getLineStartOffset(1)-1);
			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		messageTextArea.append(text);
		messageTextArea.repaint();
		
		}
	}
}
