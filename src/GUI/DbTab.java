package GUI;

import javax.swing.JPanel;
import javax.swing.JTextPane;

public class DbTab extends JPanel{
	private static final long serialVersionUID = 3900937139498662123L;

	public DbTab(){
		setLayout(null);
		JTextPane txtpn = new JTextPane();
		txtpn.setEnabled(false);
		txtpn.setEditable(false);
		txtpn.setText("Implement a mechanism to store stuff in a db like sqlite");
		txtpn.setBounds(196, 128, 354, 21);
		this.add(txtpn);
	}
	
}
