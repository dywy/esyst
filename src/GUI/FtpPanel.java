package GUI;

import javax.swing.JPanel;
import javax.swing.JTextPane;

public class FtpPanel extends JPanel{
	
	private static final long serialVersionUID = -5129548912859324418L;

	public FtpPanel(){
		
		setLayout(null);
		
		JTextPane txtpnWouldntItBe = new JTextPane();
		txtpnWouldntItBe.setEnabled(false);
		txtpnWouldntItBe.setEditable(false);
		txtpnWouldntItBe.setText("Wouldn't it be nice to have FileSend-like capabilities?  (^_^)");
		txtpnWouldntItBe.setBounds(237, 65, 223, 44);
		this.add(txtpnWouldntItBe);
		
	}

}
